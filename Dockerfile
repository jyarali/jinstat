#
# PHP Dependencies
#
FROM composer as vendor

COPY database/ database/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

#
# Frontend
#
FROM node:12.16.2-alpine3.11 as frontend

RUN mkdir -p /app/public
RUN mkdir -p /app/resources/{fonts,js,sass}

COPY package.json webpack.mix.js /app/
COPY resources/fonts/ /app/resources/fonts/
COPY resources/js/ /app/resources/js/
COPY resources/sass/ /app/resources/sass/

WORKDIR /app

RUN npm install && npm run production

#
# Application
#
FROM php:7.4-apache

RUN apt-get update
RUN apt-get install -y curl vim nano build-essential libssl-dev zlib1g-dev libpng-dev libjpeg-dev libfreetype6-dev libxml2-dev libzip-dev

RUN a2enmod rewrite
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN docker-php-ext-install bcmath gd pdo_mysql mysqli soap zip

COPY . /var/www/html
COPY --from=vendor /app/vendor/ /var/www/html/vendor/
COPY --from=frontend /app/public/js/ /var/www/html/public/js/
COPY --from=frontend /app/public/css/ /var/www/html/public/css/
COPY --from=frontend /app/fonts/ /var/www/html/public/fonts/
COPY --from=frontend /app/mix-manifest.json /var/www/html/mix-manifest.json
RUN chown -R www-data:www-data /var/www
# RUN cp .env.example .env