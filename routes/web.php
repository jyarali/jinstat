<?php

use App\User;
use Illuminate\Support\Facades\Route;
use InstagramScraper\Instagram;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/jtest', function(){
                
    $instagram = new Instagram;
    $account = $instagram->getAccount('el.grand.sport');
    // $posts = $instagram->getMedias('el.grand.sport', 20);
    dd($account);
    // $user = User::find(1);
    // $posts = $user->posts;
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/stats', 'StatController@index')->name('stats');
    // will be for admin
    Route::get('/show/{user}', 'StatController@showById');
});
