<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post_stats()
    {
        return $this->hasMany('App\PostStat');
    }
    public function last_stat() {
        return $this->hasOne('App\PostStat')->latest();
    }
}
