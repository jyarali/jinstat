<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatController extends Controller
{
    public function index()
    {   
        $user = Auth::user();
        $posts = $user->posts()->with('last_stat')->orderBy('created_time', 'desc')->take(20)->get();
        $top_posts = $posts->sortByDesc('last_stat.likes')->take(3)->values()->all();
        $stats = $user->stats()->orderBy('created_at', 'desc')->get();
        $like_average = round(intval($posts->sum('last_stat.likes'))/$posts->count());
        $comment_average = round(intval($posts->sum('last_stat.comments'))/$posts->count());
        $engagement = number_format(($like_average + $comment_average) / $user->stats->last()->followers * 100, 2);
        $all_stats = $user->stats;
        $all_stats_dates = $all_stats->map(function($item){
            return Verta::instance($item->created_at)->format('Y-n-j');
        });
        $all_stats_followers = $all_stats->map(function($item){
            return $item->followers;
        });
        $all_stats_following = $all_stats->map(function($item){
            return $item->following;
        });
        $media_chart_lables = $posts->reverse()->map(function($item){
            return Verta::Instance(gmdate('Y-m-d',$item->created_time))->format('Y-n-j');
        })->values()->all();
        $media_chart_likes = $posts->reverse()->map(function($item){
            return $item->last_stat->likes;
        })->values()->all();
        $media_chart_comments = $posts->reverse()->map(function($item){
            return $item->last_stat->comments;
        })->values()->all();
        $media_chart_captions = $posts->reverse()->pluck('caption');
        return view('stats', compact('user','like_average', 'comment_average', 'all_stats_dates', 'all_stats_followers','all_stats_following', 'engagement', 'stats', 'top_posts', 'posts', 'media_chart_lables', 'media_chart_likes', 'media_chart_comments', 'media_chart_captions'));
    }

    public function showById(User $user)
    {
        $posts = $user->posts()->with('last_stat')->orderBy('created_time', 'desc')->take(20)->get();
        $top_posts = $posts->sortByDesc('last_stat.likes')->take(3)->values()->all();
        $stats = $user->stats()->orderBy('created_at', 'desc')->get();
        $like_average = round(intval($posts->sum('last_stat.likes'))/$posts->count());
        $comment_average = round(intval($posts->sum('last_stat.comments'))/$posts->count());
        $engagement = number_format(($like_average + $comment_average) / $user->stats->last()->followers * 100, 2);
        $all_stats = $user->stats;
        $all_stats_dates = $all_stats->map(function($item){
            return Verta::instance($item->created_at)->format('Y-n-j');
        });
        $all_stats_followers = $all_stats->map(function($item){
            return $item->followers;
        });
        $all_stats_following = $all_stats->map(function($item){
            return $item->following;
        });
        $media_chart_lables = $posts->reverse()->map(function($item){
            return Verta::Instance(gmdate('Y-m-d',$item->created_time))->format('Y-n-j');
        })->values()->all();
        $media_chart_likes = $posts->reverse()->map(function($item){
            return $item->last_stat->likes;
        })->values()->all();
        $media_chart_comments = $posts->reverse()->map(function($item){
            return $item->last_stat->comments;
        })->values()->all();
        $media_chart_captions = $posts->reverse()->map(function($item){
            return strlen($item->caption);
        })->values()->all();
        return view('stats', compact('user','like_average', 'comment_average', 'all_stats_dates', 'all_stats_followers','all_stats_following', 'engagement', 'stats', 'top_posts', 'posts', 'media_chart_lables', 'media_chart_likes', 'media_chart_comments', 'media_chart_captions'));
    }
}
