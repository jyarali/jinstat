<?php

namespace App\Console;

use App\Post;
use App\PostStat;
use App\Stat;
use App\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use InstagramScraper\Instagram;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->exec("echo 'test' >> /home/jamal/Desktop/test.txt")->everyMinute();
        $schedule->call(function () {
            $users = User::all();
            foreach ($users as $user) {
                $instagram = new Instagram;
                $username = $user->insta_id;
                $account = $instagram->getAccount($username);

                $user->profile_pic = $account['profilePicUrl'];
                $user->biography = $account['biography'];
                $user->name = $account['fullName'];
                $user->save();

                // if ($account->isPrivate){
                //     return 'Sorry! account is private';
                // }

                $stat = new Stat([
                    'followers' => $account['followedByCount'],
                    'following' => $account['followsCount'],
                    'posts_count' => $account['mediaCount'],
                ]);
                $user->stats()->save($stat);

                $posts = $instagram->getMedias($username, 20);

                foreach ($posts as $post) {
                    $db_post = Post::where('mediaId', $post['id'])->first();
                    if (!$db_post) {
                        $item = new Post([
                            'mediaId' => $post['id'],
                            'type' => $post['type'],
                            'link' => $post['link'],
                            'caption' => $post['caption'],
                            'created_time' => $post['createdTime'],
                            'pic' =>  $post['imageThumbnailUrl'],
                        ]);
                        $user->posts()->save($item);
                        $new_post_stat = new PostStat([
                            'comments' => $post['commentsCount'],
                            'likes' => $post['likesCount'],
                            'video_views' => $post['videoViews'],
                        ]);
                        $item->post_stats()->save($new_post_stat);
                    } else {
                        $new_post_stat = new PostStat([
                            'comments' => $post['commentsCount'],
                            'likes' => $post['likesCount'],
                            'video_views' => $post['videoViews'],
                        ]);
                        $db_post->post_stats()->save($new_post_stat);
                    }
                }
            }
        })->dailyAt('00:00')->timezone('Asia/Tehran');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
