@extends('layouts.app')

@section('content')
<section class="general py-2 mb-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-2">
                <img width="120px" height="120px" class="rounded-circle" src="{{ $user->profile_pic }}" alt="{{ $user->name }}">
            </div>
            <div class="col-md-4">
                <p>
                    {{ $user->insta_id }}@
                </p>
                <p>
                    {{ $user->name }}
                </p>
                <p>
                    {{ $user->biography }}
                </p>
            </div>
            <div class="col-md-6">
                <div class="row align-items-center">
                    <div class="col">
                        <h6>دنبال کنندگان :</h6>
                        <span class="h2 number"> {{ $user->stats->last()->followers }}</span>
                    </div>
                    <div class="col">
                        <h6>دنبال شونده ها : </h6>
                        <span class="h2 number">{{ $user->stats->last()->following }}</span>
                    </div>
                    <div class="col">
                        <h6>پست ها :</h6>
                        <span class="h2 number"> {{ $user->stats->first()->posts_count }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="average py-3">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>درصد مشارکت  <i class="fas fa-percentage"></i></h3>
            </div>
            <div class="col">
            <h1>{{ $engagement }} درصد </h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h3>متوسط تعداد لایک ها  <i class="fas fa-heart text-danger"></i></h3>
            </div>
            <div class="col">
            <h1 class="number">{{ $like_average }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h3>متوسط تعداد کامنت ها  <i class="fas fa-comments text-secondary"></i></h3>
            </div>
            <div class="col">
            <h1 class="number">{{ $comment_average }}</h1>
            </div>
        </div>
    </div>
</section>

<section class="followers py-2 my-5">
    <div class="container">
        <h2 class="section-title">
            آمار دنبال کنندگان
        </h2>
        <canvas id='followers_chart'></canvas>
    </div>
</section>

<section class="followers py-2 my-5">
    <div class="container">
        <h2 class="section-title">
            آمار دنبال شونده ها
        </h2>
        <canvas id='following_chart'></canvas>
    </div>
</section>

<section class="summary py-2 mb-5">
    <div class="container">
        <h1>خلاصه آمار حساب کاربری</h1>
        <h5 class="text-muted mb-4">نمایش آمار 20 روز اخیر</h5>
        <table class="table table-hover text-center" id="summary">
            <thead>
              <tr class="bg-secondary text-light">
                <th scope="col">تاریخ</th>
                <th scope="col"></th>
                <th scope="col">دنبال کنندگان</th>
                <th scope="col"></th>
                <th scope="col">دنبال شوندگان</th>
                <th scope="col"></th>
                <th scope="col">پست ها</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < count($stats); $i++)
                    <tr>
                        <td>{{ Verta::instance($stats[$i]->created_at)->format('Y-n-j') }}</td>
                        <td>{{ Verta::instance($stats[$i]->created_at)->format('l') }}</td>
                        <td class="followers number">{{ $stats[$i]->followers }}</td>
                        <td class="new_followers">
                        @if($i != count($stats)-1)
                            <span class="number">
                                {{intval($stats[$i]->followers - $stats[$i+1]->followers)}}
                            </span>
                        @endif
                        </td>
                        <td class="following number">{{ $stats[$i]->following }}</td>
                        <td class="new_following">
                        @if($i != count($stats)-1)
                        <span class="number">
                            {{intval($stats[$i]->following - $stats[$i+1]->following)}}
                        </span>
                        @endif
                        </td>
                        <td class="posts_count number">{{ $stats[$i]->posts_count }}</td>
                        <td class="new_posts">
                        @if($i != count($stats)-1)
                        <span class="number">
                            {{intval($stats[$i]->posts_count - $stats[$i+1]->posts_count)}}
                        </span>
                        @endif
                        </td>
                    </tr>
                @endfor
                
            </tbody>
        </table>
    </div>
</section>

<section class="top-posts py-2 mb-5">
    <div class="container">
        <h1>پست‌های پرطرفدار</h1>
        <h5 class="text-muted mb-4">نمایش برترین‌ها از 20 پست اخیر</h5>
        <div class="row">
            @foreach ($top_posts as $post)    
            <div class="col-sm-12 col-md-6 col-lg-4">
                <iframe class="embed-responsive-item iframe"  src="{{$post->link}}/embed/" frameborder="0" allowfullscreen></iframe>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="post-summary py-2">
    <div class="container">
        <h1>خلاصه آمار پست‌ها</h1>
        <h5 class="text-muted mb-4">نمایش آمار 20 پست اخیر</h5>
        <table class="table table-hover text-center font-weight-bold" id="summary">
            <thead>
              <tr class="bg-secondary text-light">
                <th scope="col">نوع پست</th>
                <th scope="col"></th>
                <th scope="col">تاریخ ارسال</th>
                <th scope="col">کپشن</th>
                <th scope="col">لایک ها</th>
                <th scope="col">کامنت ها</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td>
                            <a target="_blank" href="{{ $post->link }}">
                            @switch($post->type)
                                @case('video')
                                        <i class="fas fa-video"data-toggle="tooltip" data-placement="top" title="ویدیو"></i>
                                    @break
                                @case('sidecar')
                                    <i class="fas fa-images"data-toggle="tooltip" data-placement="top" title="اسلاید"></i>
                                    @break
                                @default
                                    <i class="fas fa-image"data-toggle="tooltip" data-placement="top" title="عکس"></i>
                            @endswitch
                            </a>
                        </td>
                        <td>
                            <img width="40px" height="40px" class="rounded-circle" src="{{ $post->pic }}" data-toggle="tooltip" data-placement="top" data-html="true" title="<img width='200px' height='200px' style='max-width:100%' src='{{ $post->pic }}'>">
                        </td>
                        <td>{{ Verta::Instance(gmdate('Y-m-d',$post->created_time))->format('Y-n-j') }}</td>
                        <td>{{ Str::limit($post->caption, 80) }}</td>
                        <td><i class="fas fa-heart text-danger"></i> <span class="number">{{ $post->last_stat->likes }}</span></td>
                        <td><i class="fas fa-comments"></i> <span class="number">{{ $post->last_stat->comments }}</span></td>
                        
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</section>

<section class="posts_stat">
    <div class="container">
        <h1 class="mb-4">نمودار آمار پست‌ها</h1>
        <canvas id="media_chart"></canvas>
    </div>
</section>

@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script>

let number_format=(number,decimals,dec_point='.',thousands_point=',')=>{if(number==null||!isFinite(number)){throw new TypeError("number is not valid");}
if(!decimals){let len=number.toString().split('.').length;decimals=len>1?len:0;}
number=parseFloat(number).toFixed(decimals);number=number.replace('.',dec_point);let splitNum=number.split(dec_point);splitNum[0]=splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g,thousands_point);number=splitNum.join(dec_point);return number;}
Chart.defaults.global.elements.line.borderWidth = 4;
Chart.defaults.global.elements.point.radius = 3;
Chart.defaults.global.elements.point.borderWidth = 7;


let followers_chart_context = document.getElementById('followers_chart').getContext('2d');

let gradient = followers_chart_context.createLinearGradient(0, 0, 0, 250);
gradient.addColorStop(0, 'rgba(43, 227, 155, 0.6)');
gradient.addColorStop(1, 'rgba(43, 227, 155, 0.05)');

new Chart(followers_chart_context, {
    type: 'line',
    data: {
        labels: @json($all_stats_dates),
        datasets: [{
            label: "دنبال کنندگان",
            data: @json($all_stats_followers),
            backgroundColor: gradient,
            borderColor: '#2BE39B',
            fill: true
        }]
    },
    options: {
        tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
                label: (tooltipItem, data) => {
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                    return `${number_format(value, 0, '.',  ',')} ${data.datasets[tooltipItem.datasetIndex].label}`;
                }
            }
        },
        title: {
            text: "نمودار دنبال کنندگان بر اساس تاریخ",
            display: true
        },
        legend: {
            display: false
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    userCallback: (value, index, values) => {
                        if(Math.floor(value) === value) {
                            return number_format(value, 0, '.', ',');
                        }
                    },
                    suggestedMin: Math.min.apply(this, @json($all_stats_followers)) - 5,
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
let following_chart_context = document.getElementById('following_chart').getContext('2d');

    gradient = following_chart_context.createLinearGradient(0, 0, 0, 250);
    gradient.addColorStop(0, 'rgba(62, 193, 255, 0.6)');
    gradient.addColorStop(1, 'rgba(62, 193, 255, 0.05)');

    new Chart(following_chart_context, {
        type: 'line',
        data: {
            labels: @json($all_stats_dates),
            datasets: [{
                label: "دنبال شونده ها",
                data: @json($all_stats_following),
                backgroundColor: gradient,
                borderColor: '#3ec1ff',
                fill: true
            }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                        return `${number_format(value, 0, '.',  ',')} ${data.datasets[tooltipItem.datasetIndex].label}`;
                    }
                }
            },
            title: {
                text: "نمودار دنبال شوندگان بر اساس تاریخ",
                display: true
            },
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        userCallback: (value, index, values) => {
                            if(Math.floor(value) === value) {
                                return number_format(value, 0, '.', ',');
                            }
                        },
                        suggestedMin: Math.min.apply(this, @json($all_stats_following)) - 5,
                    },
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
    let media_chart_context = document.getElementById('media_chart').getContext('2d');
    new Chart(media_chart_context, {
        type: 'line',
        data: {
            labels: @json($media_chart_lables),
            datasets: [{
                label: "لایک ها",
                data: @json($media_chart_likes),
                backgroundColor: '#ED4956',
                borderColor: '#ED4956',
                fill: false
            },
                {
                    label: "کامنت ها",
                    data: @json($media_chart_comments),
                    backgroundColor: '#2caff7',
                    borderColor: '#2caff7',
                    fill: false
                },
                {
                    label: "تعداد حروف کپشن",
                    data: @json($media_chart_captions),
                    backgroundColor: '#25f7b1',
                    borderColor: '#25f7b1',
                    fill: false
                }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                        return `${number_format(value, 0, '.',  ',')} ${data.datasets[tooltipItem.datasetIndex].label}`;
                    }
                }
            },
            title: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        userCallback: (value, index, values) => {
                            if(Math.floor(value) === value) {
                                return number_format(value, 0, '.', ',');
                            }
                        }
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
$(document).ready(function(){
    $('#summary tbody tr').each(function() {
        var current = $(this);
        var new_followers = parseInt(current.find(".new_followers span").html());
        var new_following = parseInt(current.find(".new_following span").html());
        var new_posts = parseInt(current.find(".new_posts span").html());
        if (new_followers) {
            if(new_followers > 0) {
                current.find('.new_followers span').addClass('text-success').text(new_followers);
            } else {
                current.find('.new_followers span').addClass('text-danger');
            }
        }
        if (new_following) {
            if(new_following > 0) {
                current.find('.new_following span').addClass('text-success').text(new_following);
            } else {
                current.find('.new_following span').addClass('text-danger');
            }
        }
        if (new_posts) {
            if(new_posts > 0) {
                current.find('.new_posts span').addClass('text-success').text(new_posts);
            } else {
                current.find('.new_posts span').addClass('text-danger');
            }
        }
    });
    $('.number').each(function(){
        var value = $(this).text();
        var newVal = number_format(value, 0, '.',  ',');
        $(this).text(newVal);
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
});
</script>
@endsection